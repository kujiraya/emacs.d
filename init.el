;;ウィンドウ移動
(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <down>")  'windmove-down)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <right>") 'windmove-right)
;;theme設定
(add-to-list 'custom-theme-load-path (expand-file-name (concat user-emacs-directory "themes")))
;(load-theme 'manoj-dark t)
(load-theme 'apribase t)
;;スタートアップ非表示
(setq inhibit-startup-screen t)
;;キーバインド
(define-key global-map "\C-z" 'undo)
;;; バックアップファイルを作らない
(setq backup-inhibited t)
;; ビープ音を消す
(setq visible-bell t)
(setq ring-bell-function (lambda ()()) )
;;クリップボード
(setq x-select-enable-clipboard t)
(setq load-path (cons "~/.emacs.d/elisp" load-path))
;;package
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(package-initialize)

(require 'install-elisp)
(setq install-elisp-repository-directory "~/.emacs.d/elisp/")
;(setq load-path (cons "~/.emacs.d/elpa" load-path))

(when (require 'auto-complete-config nil t)
  (add-to-list 'ac-dictionary-directories "~/.emacs.d/dict")
  (setq ac-ignore-case t)
  (ac-config-default))

;;slime
(load (expand-file-name "~/quicklisp/slime-helper.el"))
;; Clozure CLをデフォルトのCommon Lisp処理系に設定
(setq inferior-lisp-program "/usr/local/bin/sbcl")


;;ac-slime
;(require 'ac-slime)
;(add-hook 'slime-mode-hook 'set-up-slime-ac)
;(add-hook 'slime-repl-mode-hook 'set-up-slime-ac)

;(require 'flex-autopair)
;(flex-autopair-mode 1)

;;tuareg
(add-to-list 'load-path (expand-file-name "~/.emacs.d/elisp/tuareg"))
(autoload 'tuareg-mode "tuareg" "Major mode for editing Caml code" t)
(autoload 'ocamldebug "ocamldebug" "Run the Caml debugger" t)
(autoload 'tuareg-imenu-set-imenu "tuareg-imenu" 
"Configuration of imenu for tuareg" t)
(add-hook 'tuareg-mode-hook 'tuareg-imenu-set-imenu)
(setq auto-mode-alist 
    (append '(("\\.ml[ily]?$" . tuareg-mode)
              ("\\.topml$" . tuareg-mode))
              auto-mode-alist))

;;path
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))
;;改行
(setq fill-column 80)
(setq text-mode-hook 'turn-on-auto-fill)
(setq default-major-mode 'text-mode)
;;行数
(global-linum-mode t)
;;透明
(set-frame-parameter nil 'alpha 90)
;; tabbar.el
(require 'tabbar)
(if (display-graphic-p)
(tabbar-mode 1))
;; グループ化しない
(setq tabbar-buffer-groups-function nil)
;; 左に表示されるボタンを無効化
(dolist (btn '(tabbar-buffer-home-button
               tabbar-scroll-left-button
               tabbar-scroll-right-button))
  (set btn (cons (cons "" nil)
                 (cons "" nil))))
;; タブ同士の間隔
(setq tabbar-separator '(0.8))
;; 外観変更
(set-face-attribute
 'tabbar-default nil
 :family (face-attribute 'default :family)
 :background (face-attribute 'mode-line-inactive :background)
 :height 0.9)
(set-face-attribute
 'tabbar-unselected nil
 :background (face-attribute 'mode-line-inactive :background)
 :foreground (face-attribute 'mode-line-inactive :foreground)
 :box nil)
(set-face-attribute
 'tabbar-selected nil
 :background (face-attribute 'mode-line :background)
 :foreground (face-attribute 'mode-line :foreground)
 :box nil)
;;折り返し
(global-visual-line-mode)
;;ツールバー
(tool-bar-mode -1)	
;;フォント
(when (display-graphic-p)
;; Monaco 12pt をデフォルトにする
(set-face-attribute 'default nil
                    :family "Inconsolata"
                    :height 140)
;; 日本語をヒラギノ角ゴProNにする
(set-fontset-font "fontset-default"
                  'japanese-jisx0208
                  '("Hiragino Maru Gothic ProN"))
;; 半角カナをヒラギノ角ゴProNにする
(set-fontset-font "fontset-default"
                  'katakana-jisx0201
                  '("Hiragino Maru Gothic ProN"))
)
;; 1行ずつスクロール
(setq scroll-conservatively 35
      scroll-margin 0
      scroll-step 1)
(setq comint-scroll-show-maximum-output t) ;; shell-mode

;; Ctrl-Tab, Ctrl-Shift-Tab でタブを切り替える
(dolist (func '(tabbar-mode tabbar-forward-tab tabbar-forward-group tabbar-backward-tab tabbar-backward-group))
  (autoload func "tabbar" "Tabs at the top of buffers and easy control-tab navigation"))
(defmacro defun-prefix-alt (name on-no-prefix on-prefix &optional do-always)
  `(defun ,name (arg)
     (interactive "P")
     ,do-always
     (if (equal nil arg)
         ,on-no-prefix
       ,on-prefix)))
(defun-prefix-alt shk-tabbar-next (tabbar-forward-tab) (tabbar-forward-group) (tabbar-mode 1))
(defun-prefix-alt shk-tabbar-prev (tabbar-backward-tab) (tabbar-backward-group) (tabbar-mode 1))
(global-set-key [(control tab)] 'shk-tabbar-next)
(global-set-key [(control shift tab)] 'shk-tabbar-prev)

;;multiple-cursors
 (require 'multiple-cursors)
;; リージョンと一致する箇所で現在行より下にあるもの1つを追加
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
;; リージョンと一致する箇所で現在行より上にあるもの1つを追加
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
;;cua-mode
(cua-mode t)
(setq cua-enable-cua-keys nil)

;;foreign-regexp
(require 'foreign-regexp)
(custom-set-variables
'(foreign-regexp/regexp-type 'perl) ;; Choose by your preference.
'(reb-re-syntax 'foreign-regexp)) ;; Tell re-builder to use foreign regexp.
