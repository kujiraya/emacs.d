;;; slime-clj-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "slime-clj" "slime-clj.el" (21114 54981 0 0))
;;; Generated autoloads from slime-clj.el

(add-hook 'slime-load-hook (lambda nil (require 'slime-clj) (slime-clj-init)))

;;;***

;;;### (autoloads nil nil ("slime-clj-pkg.el") (21114 54981 131089
;;;;;;  0))

;;;***

(provide 'slime-clj-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; slime-clj-autoloads.el ends here
